package com.example.toptal_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ToptalServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ToptalServerApplication.class, args);
    }

}
