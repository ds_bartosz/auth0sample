package com.example.toptal_server.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
@CrossOrigin(origins = "*")
public class ApiController {

    @GetMapping("sample")
    public String getSample() {
        return "Hello from the server!";
    }
}
