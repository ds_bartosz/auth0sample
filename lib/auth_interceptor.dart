import 'package:auth0_flutter/auth0_flutter.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:sample/auth_session.dart';
import 'package:sample/auth_session_provider.dart';

class AuthInterceptor extends Interceptor {
  final FlutterSecureStorage secureStorage;
  final Auth0 auth0;
  final Dio dio;

  AuthInterceptor({
    required this.secureStorage,
    required this.auth0,
    required this.dio,
  });

  @override
  Future onError(DioException err, ErrorInterceptorHandler handler) async {
    print('intercepted error');
    if (err.response?.statusCode == 401) {
      final sessionString = await secureStorage.read(key: 'session');
      if (sessionString == null) {
        return err;
      }

      final authSession = AuthSession.fromJsonString(sessionString);
      final refreshToken = authSession.refreshToken;
      if (authSession.state == AuthSessionState.loggedOut ||
          refreshToken == null) {
        return err;
      }

      if (!err.requestOptions.extra['refreshToken']) {
        return err;
      }

      try {
        final credentials =
            await auth0.api.renewCredentials(refreshToken: refreshToken);
        final newSession = AuthSession.loggedIn(credentials);
        AuthSessionProvider.session = newSession;

        RequestOptions options = err.response!.requestOptions;
        options.headers['Authorization'] = 'Bearer ${newSession.token}';
        options.extra['refreshToken'] = false;
        return dio.fetch(options);
      } catch (err, stack) {
        print(err);
        print(stack);
        return err;
      }
    }

    return err;
  }
}
