import 'dart:convert';

import 'package:auth0_flutter/auth0_flutter.dart';

class AuthSession {
  final AuthSessionState state;
  final String? token;
  final String? refreshToken;
  final UserProfile? userProfile;

  AuthSession({
    required this.state,
    this.token,
    this.refreshToken,
    this.userProfile,
  });

  Map<String, dynamic> toJson() => {
        'state': state,
        'token': token,
        'refreshToken': refreshToken,
        'userProfile': userProfile?.toJson(),
      };

  String toJsonString() => json.encode(toJson(), toEncodable: (value) {
        if (value is UserProfile) {
          return value.toJson();
        }

        if (value is AuthSessionState) {
          return serialize(value);
        }
        return value;
      });

  factory AuthSession.fromJsonString(final String json) =>
      AuthSession.fromJson(jsonDecode(json));

  factory AuthSession.fromJson(final Map<String, dynamic> result) {
    try {
      final state = deserialize(result['state']);
      final token = result['token'];
      final refreshToken = result['refreshToken'];
      final userProfile = result['userProfile'] != null
          ? UserProfile.fromMap(result['userProfile'])
          : null;
      return AuthSession(
        state: state,
        token: token,
        refreshToken: refreshToken,
        userProfile: userProfile,
      );
    } catch (ex) {
      return AuthSession.loggedOut();
    }
  }

  factory AuthSession.loggedOut() =>
      AuthSession(state: AuthSessionState.loggedOut);

  factory AuthSession.loggedIn(Credentials credentials) => AuthSession(
      state: AuthSessionState.loggedIn,
      token: credentials.idToken,
      userProfile: credentials.user,
      refreshToken: credentials.refreshToken);
}

enum AuthSessionState {
  loggedIn,
  loggedOut,
}

String serialize(AuthSessionState state) {
  switch (state) {
    case AuthSessionState.loggedIn:
      return 'loggedIn';
    case AuthSessionState.loggedOut:
      return 'loggedOut';
    default:
      return 'loggedOut';
  }
}

AuthSessionState deserialize(String? state) {
  switch (state) {
    case 'loggedIn':
      return AuthSessionState.loggedIn;
    case 'loggedOut':
      return AuthSessionState.loggedOut;
    default:
      return AuthSessionState.loggedOut;
  }
}

extension UserProfileToMap on UserProfile {
  // factory UserProfile.fromJson(final Map<String, dynamic> json) =>
  //     UserProfile.fromMap(json);

  Map<String, dynamic> toJson() => {
        'sub': sub,
        'name': name,
        'given_name': givenName,
        'family_name': familyName,
        'middle_name': middleName,
        'nickname': nickname,
        'preferred_username': preferredUsername,
        'profile': profileUrl?.toString(),
        'picture': pictureUrl?.toString(),
        'website': websiteUrl?.toString(),
        'email': email,
        'email_verified': isEmailVerified,
        'gender': gender,
        'birthdate': birthdate,
        'zoneinfo': zoneinfo,
        'locale': locale,
        'phone_number': phoneNumber,
        'phone_number_verified': isPhoneNumberVerified,
        'address': address,
        'updated_at': updatedAt?.toIso8601String(),
        'custom_claims': customClaims,
      };
}
