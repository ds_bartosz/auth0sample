import 'dart:async';

import 'package:auth0_flutter/auth0_flutter.dart';
import 'package:auth0_flutter/auth0_flutter_web.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:sample/auth_interceptor.dart';
import 'package:sample/auth_session.dart';
import 'package:sample/auth_session_provider.dart';

import 'constants.dart';
import 'hero.dart';
import 'user.dart';

class ExampleApp extends StatefulWidget {
  final Auth0? auth0;

  const ExampleApp({this.auth0, final Key? key}) : super(key: key);

  @override
  State<ExampleApp> createState() => _ExampleAppState();
}

class _ExampleAppState extends State<ExampleApp> {
  String? serverResult;

  final secureStorage = const FlutterSecureStorage();
  late Dio client;
  late Auth0 auth0;
  late Auth0Web auth0Web;

  @override
  void initState() {
    super.initState();

    auth0 = widget.auth0 ??
        Auth0(dotenv.env['AUTH0_DOMAIN']!, dotenv.env['AUTH0_CLIENT_ID']!);
    auth0Web =
        Auth0Web(dotenv.env['AUTH0_DOMAIN']!, dotenv.env['AUTH0_CLIENT_ID']!);

    client = Dio();
    client.interceptors.add(AuthInterceptor(
      secureStorage: secureStorage,
      auth0: auth0,
      dio: client,
    ));

    if (kIsWeb) {
      auth0Web.onLoad().then((final credentials) => setState(() {
            AuthSessionProvider.session = credentials == null
                ? AuthSession.loggedOut()
                : AuthSession.loggedIn(credentials);
          }));
    } else {
      //mobile apps
      secureStorage.read(key: 'session').then((sessionJson) {
        if (sessionJson != null) {
          setState(() {
            AuthSessionProvider.session =
                AuthSession.fromJsonString(sessionJson);
          });
        } else {
          setState(() {
            AuthSessionProvider.session = AuthSession.loggedOut();
          });
        }
      });
    }
  }

  Future<void> login() async {
    try {
      if (kIsWeb) {
        return auth0Web.loginWithRedirect(redirectUrl: 'http://localhost:3000');
      }

      var credentials = await auth0
          .webAuthentication(scheme: dotenv.env['AUTH0_CUSTOM_SCHEME'])
          .login(useEphemeralSession: true);
      final session = AuthSession.loggedIn(credentials);
      secureStorage.write(key: 'session', value: session.toJsonString());

      setState(() {
        AuthSessionProvider.session = session;
      });
    } catch (e, stack) {
      print(e);
      print(stack);
    }
  }

  Future<void> logout() async {
    try {
      if (kIsWeb) {
        await auth0Web.logout(returnToUrl: 'http://localhost:3000');
      } else {
        auth0.credentialsManager.clearCredentials();
        unawaited(secureStorage.delete(key: 'session'));
        setState(() {
          AuthSessionProvider.session = AuthSession.loggedOut();
        });
      }
    } catch (e) {
      print(e);
    }
  }

  Future<String> issueServerCall() async {
    try {
      final token = AuthSessionProvider.session?.token;
      Map<String, String> headers = {'Authorization': 'Bearer $token'};
      Response result = await client.get('http://localhost:8085/api/sample',
          options: Options(headers: headers, extra: {'refreshToken': true}));
      return 'get result -> $result';
    } catch (e) {
      return e.toString();
    }
  }

  Future<String> issueServerCall2() async {
    try {
      final token = AuthSessionProvider.session?.token;
      Map<String, String> headers = {'Authorization': 'Bearer x$token'};
      Response result = await client.get('http://localhost:8085/api/sample',
          options: Options(headers: headers, extra: {'refreshToken': true}));
      return 'get result -> $result';
    } catch (e) {
      if (e is DioException && e.response?.statusCode == 401) {
        await logout();
        return 'Unauthorized';
      }
      return e.toString();
    }
  }

  @override
  Widget build(final BuildContext context) {
    final _user = AuthSessionProvider.session?.userProfile;
    return MaterialApp(
      home: Scaffold(
          body: AuthSessionProvider.session == null
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : Padding(
                  padding: const EdgeInsets.only(
                    top: padding,
                    bottom: padding,
                    left: padding / 2,
                    right: padding / 2,
                  ),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                            child: Row(children: [
                          _user != null
                              ? Expanded(
                                  child: Column(
                                  children: [
                                    UserWidget(user: _user),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 20.0),
                                      child: Text(serverResult ?? '**'),
                                    ),
                                    ElevatedButton(
                                      onPressed: () async {
                                        String result = await issueServerCall();
                                        setState(() {
                                          serverResult = result;
                                        });
                                      },
                                      style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.black),
                                      ),
                                      child: const Text(
                                          'Call Server with valid token'),
                                    ),
                                    ElevatedButton(
                                      onPressed: () async {
                                        String result =
                                            await issueServerCall2();
                                        setState(() {
                                          serverResult = result;
                                        });
                                      },
                                      style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.black),
                                      ),
                                      child: const Text(
                                          'Call Server with invalid token'),
                                    )
                                  ],
                                ))
                              : const Expanded(child: HeroWidget())
                        ])),
                        _user != null
                            ? ElevatedButton(
                                onPressed: logout,
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.black),
                                ),
                                child: const Text('Logout'),
                              )
                            : ElevatedButton(
                                onPressed: login,
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.black),
                                ),
                                child: const Text('Login'),
                              )
                      ]),
                )),
    );
  }
}
